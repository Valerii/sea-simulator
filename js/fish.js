var timerMiliseconds;
var Sharks = new Array();
var Fishes = new Array();
var fullElements = new Array();

var NSharks;
var sharkBornPeriod;
var sharkLifeTime;
var sharkHungryTime;
var sharkColor = '#FF5353';

var NFishes;
var fishBornPeriod;
var fishLifeTime;
var fishColor = '#73BDFF';

// Variables for canvas.
var sharkSumCoordinates;
var fishSumCoordinates;
var timeCounter;
var MiuShark = new Array();
var MiuFish = new Array();

// Adding size of one square side.
var SquareSideSize;
var K;

var timer;

// Function insert parameters.
function insertParameters() {
    Sharks = new Array();
    Fishes = new Array();
    fullElements = new Array();
    timeCounter = 10;

    SquareSideSize = parseInt(document.getElementById('SquareSideSize').value);
    timerMiliseconds = parseInt(document.getElementById('timerMiliseconds').value);

    // Sharks parameters.
    NSharks = parseInt(document.getElementById('NSharks').value);
    sharkBornPeriod = parseInt(document.getElementById('sharkBornPeriod').value);
    sharkLifeTime = parseInt(document.getElementById('sharkLifeTime').value);
    sharkHungryTime = parseInt(document.getElementById('sharkHungryTime').value);

    // Fishes parameters.
    NFishes = parseInt(document.getElementById('NFishes').value);
    fishBornPeriod = parseInt(document.getElementById('fishBornPeriod').value);
    fishLifeTime = parseInt(document.getElementById('fishLifeTime').value);

    // Getting all desk and inserting into it empty div elements.
    desk = document.getElementById("desk");
    desk.innerHTML = '';
    desk.style.width = '500px';
    deskWidth = desk.clientWidth;
    SquareElementWidth = 100/SquareSideSize;
    for (var i=1; i<=SquareSideSize; i++) {
        fullElements[i] = [];
        for (var j=1; j<=SquareSideSize; j++) {
            squareElement = document.createElement("DIV");
            squareElement.id = i+"|"+j;
            squareElement.style.width = SquareElementWidth + "%";
            squareElement.style.height = SquareElementWidth + "%";
            squareElement.style.float = 'left';
            squareElement.style.border = '1px dotted #cccccc';
            squareElement.style.boxSizing = 'border-box';
            desk.appendChild(squareElement);

            fullElements[i][j] = 0;
        }
    }
    // Recounting deskWidth for escape Chrome problem.
    deskWidth = squareElement.clientWidth*SquareSideSize;
    desk.style.width = deskWidth + 4*SquareSideSize + 'px';
    desk.style.height = deskWidth + 4*SquareSideSize + 'px';

    // Placing sharks on the board.
    rowRandom = getRandomInt(1, SquareSideSize);
    colRandom = getRandomInt(1, SquareSideSize);

    for (var i=0; i<NSharks; i++) {
        // Object Shark has it coordinates.
        Sharks[i] = {};
        Sharks[i].rowN = rowRandom;
        Sharks[i].colN = colRandom;
        Sharks[i].sharkLifeTime = 0;
        Sharks[i].sharkHungryTime = -1;
        paintAnimal(rowRandom, colRandom, sharkColor); // Paint its coordinates.
        fullElements[rowRandom][colRandom] = 1; // Marking it on the desk array.

        // Finding next random coordinates for next cycle.
        rowRandom = getRandomInt(1, SquareSideSize);
        colRandom = getRandomInt(1, SquareSideSize);
        while(fullElements[rowRandom][colRandom] != 0) {
            rowRandom = getRandomInt(1, SquareSideSize);
            colRandom = getRandomInt(1, SquareSideSize);
        }
    }

    // Placing fishes on the board.
    for (var i=0; i<NFishes; i++) {
        // Object Fish has it coordinates.
        Fishes[i] = {};
        Fishes[i].rowN = rowRandom;
        Fishes[i].colN = colRandom;
        Fishes[i].fishLifeTime = 0;
        // Paint its coordinates.
        paintAnimal(rowRandom, colRandom, fishColor);
        // Marking it on the desk array.
        fullElements[rowRandom][colRandom] = 2;

        // Finding next random coordinates for next cycle.
        rowRandom = getRandomInt(1, SquareSideSize);
        colRandom = getRandomInt(1, SquareSideSize);
        while(fullElements[rowRandom][colRandom] != 0) {
            rowRandom = getRandomInt(1, SquareSideSize);
            colRandom = getRandomInt(1, SquareSideSize);
        }
    }

    // Drawing static graph.
    var canvas = document.getElementById("static_graph");
    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.beginPath();
    // Context style.
    context.strokeStyle = "#000";
    context.lineWidth = 3;
    // X coordinate.
    context.moveTo(5, 390);
    context.lineTo(990, 390);
    context.stroke();

    // Y coordinate.
    context.beginPath();
    context.moveTo(10, 10);
    context.lineTo(10, 395);
    context.stroke();

    K = SquareSideSize*SquareSideSize;

    // Drawing graphs.
    var canvas = document.getElementById("live_graph");
    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.beginPath();
    // Context style.
    context.strokeStyle = "#000";
    context.lineWidth = 3;
    // X coordinate.
    context.moveTo(5, 390);
    context.lineTo(990, 390);
    context.stroke();

    // Y coordinate.
    context.beginPath();
    context.moveTo(10, 10);
    context.lineTo(10, 395);
    context.stroke();

    sharkSumCoordinates = [10, 390-NSharks];
    fishSumCoordinates = [10, 390-NFishes];
}

// Starting live process.
var newFishChild = new Array();
var deleteFishArray = new Array();
var newSharkChild = new Array();
var deleteSharkArray = new Array();

function startLiveProcess() {
    // Moving for sharks.
    Sharks.forEach(function(eachShark) {
        sharkRowN = eachShark.rowN;
        sharkColN = eachShark.colN;
        if ((sharkRowN == 1) && (sharkColN == 1)) {
            // Left top corner.
            if (fullElements[SquareSideSize][sharkColN] == 2) {
                eachShark.rowN = SquareSideSize;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][SquareSideSize] == 2) {
                eachShark.colN = SquareSideSize;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][sharkColN+1] == 2) {
                eachShark.colN = sharkColN+1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN+1][sharkColN] == 2) {
                eachShark.rowN = sharkRowN+1;
                eachShark.sharkHungryTime = 0;
            }
            else {
                // If there is no food for shark on the top left corner.
                do {
                    direction = getRandomInt(1, 4);
                    repeat = 0;
                    if ((direction == 1) && (fullElements[SquareSideSize][sharkColN] == 0)) {
                        eachShark.rowN = SquareSideSize;
                        break;
                    }
                    else if ((direction == 2) && (fullElements[sharkRowN][sharkColN+1] == 0)) {
                        eachShark.colN = sharkColN+1;
                        break;
                    }
                    else if ((direction == 3) && (fullElements[sharkRowN+1][sharkColN] == 0)) {
                        eachShark.rowN = sharkRowN+1;
                        break;
                    }
                    else if ((direction == 4) && (fullElements[sharkRowN][SquareSideSize] == 0)) {
                        eachShark.colN = SquareSideSize;
                        break;
                    }
                    else if ((fullElements[SquareSideSize][sharkColN] == 0) || (fullElements[sharkRowN][sharkColN+1] == 0)
                                || (fullElements[sharkRowN+1][sharkColN] == 0) || (fullElements[sharkRowN][SquareSideSize] == 0)) {

                        repeat = 1;
                    }
                    else {
                        direction = 5;
                        break;
                    }
                } while (repeat == 1);
            }
        }
        else if ((sharkRowN == 1) && (sharkColN == SquareSideSize)) {
            // Right top corner.
            if (fullElements[SquareSideSize][sharkColN] == 2) {
                eachShark.rowN = SquareSideSize;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][1] == 2) {
                eachShark.colN = 1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][sharkColN-1] == 2) {
                eachShark.colN = sharkColN-1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN+1][sharkColN] == 2) {
                eachShark.rowN = sharkRowN+1;
                eachShark.sharkHungryTime = 0;
            }
            else {
                // If there is no food for shark on the right top corner.
                do {
                    direction = getRandomInt(1, 4);
                    repeat = 0;
                    if ((direction == 1) && (fullElements[SquareSideSize][sharkColN] == 0)) {
                        eachShark.rowN = SquareSideSize;
                        break;
                    }
                    else if ((direction == 2) && (fullElements[sharkRowN][1] == 0)) {
                        eachShark.colN = 1;
                        break;
                    }
                    else if ((direction == 3) && (fullElements[sharkRowN+1][sharkColN] == 0)) {
                        eachShark.rowN = sharkRowN+1;
                        break;
                    }
                    else if ((direction == 4) && (fullElements[sharkRowN][sharkColN-1] == 0)) {
                        eachShark.colN = sharkColN-1;
                        break;
                    }
                    else if ((fullElements[SquareSideSize][sharkColN] == 0) || (fullElements[sharkRowN][1] == 0)
                            || (fullElements[sharkRowN+1][sharkColN] == 0) || (fullElements[sharkRowN][sharkColN-1] == 0)) {

                        repeat = 1;
                    }
                    else {
                        direction = 5;
                        break;
                    }
                } while (repeat == 1);
            }
        }
        else if ((sharkRowN == SquareSideSize) && (sharkColN == 1)) {
            // Left bottom corner.
            if (fullElements[1][sharkColN] == 2) {
                eachShark.rowN = 1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][SquareSideSize] == 2) {
                eachShark.colN = SquareSideSize;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][sharkColN+1] == 2) {
                eachShark.colN = sharkColN+1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN-1][sharkColN] == 2) {
                eachShark.rowN = sharkRowN-1;
                eachShark.sharkHungryTime = 0;
            }
            else {
                // If there is no food for shark on the left bottom corner.
                do {
                    direction = getRandomInt(1, 4);
                    repeat = 0;
                    if ((direction == 1) && (fullElements[sharkRowN-1][sharkColN] == 0)) {
                        eachShark.rowN = sharkRowN-1;
                        break;
                    }
                    else if ((direction == 2) && (fullElements[sharkRowN][sharkColN+1] == 0)) {
                        eachShark.colN = sharkColN+1;
                        break;
                    }
                    else if ((direction == 3) && (fullElements[1][sharkColN] == 0)) {
                        eachShark.rowN = 1;
                        break;
                    }
                    else if ((direction == 4) && (fullElements[sharkRowN][SquareSideSize] == 0)) {
                        eachShark.colN = SquareSideSize;
                        break;
                    }
                    else if ((fullElements[sharkRowN-1][sharkColN] == 0) || (fullElements[sharkRowN][sharkColN+1] == 0)
                            || (fullElements[1][sharkColN] == 0) || (fullElements[sharkRowN][SquareSideSize] == 0)) {

                        repeat = 1;
                    }
                    else {
                        direction = 5;
                        break;
                    }
                } while (repeat == 1);
            }
        }
        else if ((sharkRowN == SquareSideSize) && (sharkColN == SquareSideSize)) {
            // Right bottom corner.
            if (fullElements[1][sharkColN] == 2) {
                eachShark.rowN = 1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][1] == 2) {
                eachShark.colN = 1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN-1][sharkColN] == 2) {
                eachShark.rowN = sharkRowN-1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][sharkColN-1] == 2) {
                eachShark.colN = sharkColN-1;
                eachShark.sharkHungryTime = 0;
            }
            else {
                // If there is no food for shark on the right bottom corner.
                do {
                    direction = getRandomInt(1, 4);
                    repeat = 0;
                    if ((direction == 1) && (fullElements[sharkRowN-1][sharkColN] == 0)) {
                        eachShark.rowN = sharkRowN-1;
                        break;
                    }
                    else if ((direction == 2) && (fullElements[sharkRowN][1] == 0)) {
                        eachShark.colN = 1;
                        break;
                    }
                    else if ((direction == 3) && (fullElements[1][sharkColN] == 0)) {
                        eachShark.rowN = 1;
                        break;
                    }
                    else if ((direction == 4) && (fullElements[sharkRowN][SquareSideSize-1] == 0)) {
                        eachShark.colN = SquareSideSize-1;
                        break;
                    }
                    else if ((fullElements[sharkRowN-1][sharkColN] == 0) || (fullElements[sharkRowN][1] == 0)
                            || (fullElements[1][sharkColN] == 0) || (fullElements[sharkRowN][SquareSideSize-1] == 0)) {

                        repeat = 1;
                    }
                    else {
                        direction = 5;
                        break;
                    }
                } while (repeat == 1);
            }
        }
        else if (sharkRowN == 1) {
            // Top line.
            if (fullElements[SquareSideSize][sharkColN] == 2) {
                eachShark.rowN = SquareSideSize;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][sharkColN+1] == 2) {
                eachShark.colN = sharkColN+1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN+1][sharkColN] == 2) {
                eachShark.rowN = sharkRowN+1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][sharkColN-1] == 2) {
                eachShark.colN = sharkColN-1;
                eachShark.sharkHungryTime = 0;
            }
            else {
                // If there is no food for shark on the top line.
                do {
                    direction = getRandomInt(1, 4);
                    repeat = 0;
                    if ((direction == 1) && (fullElements[SquareSideSize][sharkColN] == 0)) {
                        eachShark.rowN = SquareSideSize;
                        break;
                    }
                    else if ((direction == 2) && (fullElements[sharkRowN][sharkColN+1] == 0)) {
                        eachShark.colN = sharkColN+1;
                        break;
                    }
                    else if ((direction == 3) && (fullElements[sharkRowN+1][sharkColN] == 0)) {
                        eachShark.rowN = sharkRowN+1;
                        break;
                    }
                    else if ((direction == 4) && (fullElements[sharkRowN][sharkColN-1] == 0)) {
                        eachShark.colN = sharkColN-1;
                        break;
                    }
                    else if ((fullElements[SquareSideSize][sharkColN] == 0) || (fullElements[sharkRowN][sharkColN+1] == 0)
                            || (fullElements[sharkRowN+1][sharkColN] == 0) || (fullElements[sharkRowN][sharkColN-1] == 0)) {

                        repeat = 1;
                    }
                    else {
                        direction = 5;
                        break;
                    }
                } while (repeat == 1);
            }
        }
        else if (sharkRowN == SquareSideSize) {
            // Bottom line.
            if (fullElements[sharkRowN-1][sharkColN] == 2) {
                eachShark.rowN = sharkRowN-1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][sharkColN+1] == 2) {
                eachShark.colN = sharkColN+1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[1][sharkColN] == 2) {
                eachShark.rowN = 1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][sharkColN-1] == 2) {
                eachShark.colN = sharkColN-1;
                eachShark.sharkHungryTime = 0;
            }
            else {
                // If there is no food for shark on the bottom line.
                do {
                    direction = getRandomInt(1, 4);
                    repeat = 0;
                    if ((direction == 1) && (fullElements[sharkRowN-1][sharkColN] == 0)) {
                        eachShark.rowN = sharkRowN-1;
                        break;
                    }
                    else if ((direction == 2) && (fullElements[sharkRowN][sharkColN+1] == 0)) {
                        eachShark.colN = sharkColN+1;
                        break;
                    }
                    else if ((direction == 3) && (fullElements[1][sharkColN] == 0)) {
                        eachShark.rowN = 1;
                        break;
                    }
                    else if ((direction == 4) && (fullElements[sharkRowN][sharkColN-1] == 0)) {
                        eachShark.colN = sharkColN-1;
                        break;
                    }
                    else if ((fullElements[sharkRowN-1][sharkColN] == 0) || (fullElements[sharkRowN][sharkColN+1] == 0)
                            || (fullElements[1][sharkColN] == 0) || (fullElements[sharkRowN][sharkColN-1] == 0)) {

                        repeat = 1;
                    }
                    else {
                        direction = 5;
                        break;
                    }
                } while (repeat == 1);
            }
        }
        else if (sharkColN == 1) {
            // Left line.
            if (fullElements[sharkRowN-1][sharkColN] == 2) {
                eachShark.rowN = sharkRowN-1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][sharkColN+1] == 2) {
                eachShark.colN = sharkColN+1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN+1][sharkColN] == 2) {
                eachShark.rowN = sharkRowN+1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][SquareSideSize] == 2) {
                eachShark.colN = SquareSideSize;
                eachShark.sharkHungryTime = 0;
            }
            else {
                // If there is no food for shark on the left line.
                do {
                    direction = getRandomInt(1, 4);
                    repeat = 0;
                    if ((direction == 1) && (fullElements[sharkRowN-1][sharkColN] == 0)) {
                        eachShark.rowN = sharkRowN-1;
                        break;
                    }
                    else if ((direction == 2) && (fullElements[sharkRowN][sharkColN+1] == 0)) {
                        eachShark.colN = sharkColN+1;
                        break;
                    }
                    else if ((direction == 3) && (fullElements[sharkRowN+1][sharkColN] == 0)) {
                        eachShark.rowN = sharkRowN+1;
                        break;
                    }
                    else if ((direction == 4) && (fullElements[sharkRowN][SquareSideSize] == 0)) {
                        eachShark.colN = SquareSideSize;
                        break;
                    }
                    else if ((fullElements[sharkRowN-1][sharkColN] == 0) || (fullElements[sharkRowN][sharkColN+1] == 0)
                            || (fullElements[sharkRowN+1][sharkColN] == 0) || (fullElements[sharkRowN][SquareSideSize] == 0)) {

                        repeat = 1;
                    }
                    else {
                        direction = 5;
                        break;
                    }
                } while (repeat == 1);
            }
        }
        else if (sharkColN == SquareSideSize) {
            // Right line.
            if (fullElements[sharkRowN-1][sharkColN] == 2) {
                eachShark.rowN = sharkRowN-1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][1] == 2) {
                eachShark.colN = 1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN+1][sharkColN] == 2) {
                eachShark.rowN = sharkRowN+1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][sharkColN-1] == 2) {
                eachShark.colN = sharkColN-1;
                eachShark.sharkHungryTime = 0;
            }
            else {
                // If there is no food for shark on the right line.
                do {
                    direction = getRandomInt(1, 4);
                    repeat = 0;
                    if ((direction == 1) && (fullElements[sharkRowN-1][sharkColN] == 0)) {
                        eachShark.rowN = sharkRowN-1;
                        break;
                    }
                    else if ((direction == 2) && (fullElements[sharkRowN][1] == 0)) {
                        eachShark.colN = 1;
                        break;
                    }
                    else if ((direction == 3) && (fullElements[sharkRowN+1][sharkColN] == 0)) {
                        eachShark.rowN = sharkRowN+1;
                        break;
                    }
                    else if ((direction == 4) && (fullElements[sharkRowN][sharkColN-1] == 0)) {
                        eachShark.colN = sharkColN-1;
                        break;
                    }
                    else if ((fullElements[sharkRowN-1][sharkColN] == 0) || (fullElements[sharkRowN][1] == 0)
                            || (fullElements[sharkRowN+1][sharkColN] == 0) || (fullElements[sharkRowN][sharkColN-1] == 0)) {

                        repeat = 1;
                    }
                    else {
                        direction = 5;
                        break;
                    }
                } while (repeat == 1);
            }
        }
        else {
            // Center desk.
            if (fullElements[sharkRowN-1][sharkColN] == 2) {
                eachShark.rowN = sharkRowN-1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][sharkColN+1] == 2) {
                eachShark.colN = sharkColN+1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN+1][sharkColN] == 2) {
                eachShark.rowN = sharkRowN+1;
                eachShark.sharkHungryTime = 0;
            }
            else if (fullElements[sharkRowN][sharkColN-1] == 2) {
                eachShark.colN = sharkColN-1;
                eachShark.sharkHungryTime = 0;
            }
            else {
                // If there is no food for shark on the center desk.
                do {
                    direction = getRandomInt(1, 4);
                    repeat = 0;
                    if ((direction == 1) && (fullElements[sharkRowN-1][sharkColN] == 0)) {
                        eachShark.rowN = sharkRowN-1;
                        break;
                    }
                    else if ((direction == 2) && (fullElements[sharkRowN][sharkColN+1] == 0)) {
                        eachShark.colN = sharkColN+1;
                        break;
                    }
                    else if ((direction == 3) && (fullElements[sharkRowN+1][sharkColN] == 0)) {
                        eachShark.rowN = sharkRowN+1;
                        break;
                    }
                    else if ((direction == 4) && (fullElements[sharkRowN][sharkColN-1] == 0)) {
                        eachShark.colN = sharkColN-1;
                        break;
                    }
                    else if ((fullElements[sharkRowN-1][sharkColN] == 0) || (fullElements[sharkRowN][sharkColN+1] == 0)
                                || (fullElements[sharkRowN+1][sharkColN] == 0) || (fullElements[sharkRowN][sharkColN-1] == 0)) {

                        repeat = 1;
                    }
                    else {
                        direction = 5;
                        break;
                    }
                } while (repeat == 1);
            }
        }

        // Clearing place where shark was.
        paintAnimal(sharkRowN, sharkColN, '');
        fullElements[sharkRowN][sharkColN] = 0;

        // Painting place where shark will be.
        paintAnimal(eachShark.rowN, eachShark.colN, sharkColor);
        fullElements[eachShark.rowN][eachShark.colN] = 1;

        // Borning a shark child.
        if ((eachShark.sharkLifeTime != 0) && (eachShark.sharkLifeTime % sharkBornPeriod == 0) &&
            ((eachShark.sharkHungryTime == 0) || (direction != 5))) {

            var newSharkObj = {};
            newSharkObj.colN = sharkColN;
            newSharkObj.rowN = sharkRowN;
            newSharkObj.sharkLifeTime = 0;
            newSharkObj.sharkHungryTime = -1;
            paintAnimal(sharkRowN, sharkColN, sharkColor);
            newSharkChild.push(newSharkObj);
            fullElements[sharkRowN][sharkColN] = 1;
        }

        // Count shark lifetime.
        eachShark.sharkLifeTime++;

        // Shark dying.
        if ((eachShark.sharkLifeTime == sharkLifeTime) || (eachShark.sharkHungryTime == sharkHungryTime)) {
            paintAnimal(eachShark.rowN, eachShark.colN, '');
            fullElements[eachShark.rowN][eachShark.colN] = 0;
            if ((sharkKey = array_search(eachShark, Sharks)) !== false) {
                deleteSharkArray.push(sharkKey);
                //Sharks.splice(sharkKey, 1);
            }
        }
        else {
            if (eachShark.sharkHungryTime == -1) {
                eachShark.sharkHungryTime++;
            }
            eachShark.sharkHungryTime++;
        }


    });

    // Sorting an array for right deleting sharks.
    deleteSharkArray.sort(sortfunction);
    for (var i=0; i<deleteSharkArray.length; i++) {
        Sharks.splice((deleteSharkArray[i]-i), 1);
    }
    deleteSharkArray.splice(0, deleteSharkArray.length);

    for (key in newSharkChild) {
        Sharks.push(newSharkChild[key]);
    }
    newSharkChild = new Array();

    // Moving for fishes.
    Fishes.forEach(function(eachFish) {
        fishRowN = eachFish.rowN;
        fishColN = eachFish.colN;
        // Deleting fish if she was eatten.
        if (fullElements[fishRowN][fishColN] != 2) {
            if ((fishKey = array_search(eachFish, Fishes)) !== false){
                deleteFishArray.push(fishKey);
                //Fishes.splice(fishKey, 1);
            }
            return;
        }
        if ((fishRowN == 1) && (fishColN == 1)) {
            // Fish on the left top corner.
            do {
                direction = getRandomInt(1, 4);
                repeat = 0;
                if ((direction == 1) && (fullElements[SquareSideSize][fishColN] == 0)) {
                    eachFish.rowN = SquareSideSize;
                    break;
                } else if ((direction == 2) && (fullElements[fishRowN][fishColN+1] == 0)) {
                    eachFish.colN = fishColN+1;
                    break;
                } else if ((direction == 3) && (fullElements[fishRowN+1][fishColN] == 0)) {
                    eachFish.rowN = fishRowN+1;
                    break;
                } else if ((direction == 4) && (fullElements[fishRowN][SquareSideSize] == 0)) {
                    eachFish.colN = SquareSideSize;
                    break;
                } else if ((fullElements[SquareSideSize][fishColN] == 0) || (fullElements[fishRowN][fishColN+1] == 0)
                        || (fullElements[fishRowN+1][fishColN] == 0) || (fullElements[fishRowN][SquareSideSize] == 0)) {

                    repeat = 1;
                } else {
                    direction = 5;
                    break;
                }
            } while (repeat == 1);
        } else if ((fishRowN == 1) && (fishColN == SquareSideSize)) {
            // Fish on the right top corner.
            do {
                direction = getRandomInt(1, 4);
                repeat = 0;
                if ((direction == 1) && (fullElements[SquareSideSize][fishColN] == 0)) {
                    eachFish.rowN = SquareSideSize;
                    break;
                } else if ((direction == 2) && (fullElements[fishRowN][1] == 0)) {
                    eachFish.colN = 1;
                    break;
                } else if ((direction == 3) && (fullElements[fishRowN+1][fishColN] == 0)) {
                    eachFish.rowN = fishRowN+1;
                    break;
                } else if ((direction == 4) && (fullElements[fishRowN][fishColN-1] == 0)) {
                    eachFish.colN = fishColN-1;
                    break;
                } else if ((fullElements[SquareSideSize][fishColN] == 0) || (fullElements[fishRowN][1] == 0)
                        || (fullElements[fishRowN+1][fishColN] == 0) || (fullElements[fishRowN][fishColN-1] == 0)) {

                    repeat = 1;
                } else {
                    direction = 5;
                    break;
                }
            } while (repeat == 1);
        } else if ((fishRowN == SquareSideSize) && (fishColN == 1)) {
            // Fish on the left bottom corner.
            do {
                direction = getRandomInt(1, 4);
                repeat = 0;
                if ((direction == 1) && (fullElements[fishRowN-1][fishColN] == 0)) {
                    eachFish.rowN = fishRowN-1;
                    break;
                } else if ((direction == 2) && (fullElements[fishRowN][fishColN+1] == 0)) {
                    eachFish.colN = fishColN+1;
                    break;
                } else if ((direction == 3) && (fullElements[1][fishColN] == 0)) {
                    eachFish.rowN = 1;
                    break;
                } else if ((direction == 4) && (fullElements[fishRowN][SquareSideSize] == 0)) {
                    eachFish.colN = SquareSideSize;
                    break;
                } else if ((fullElements[fishRowN-1][fishColN] == 0) || (fullElements[fishRowN][fishColN+1] == 0)
                        || (fullElements[1][fishColN] == 0) || (fullElements[fishRowN][SquareSideSize] == 0)) {

                    repeat = 1;
                } else {
                    direction = 5;
                    break;
                }
            } while (repeat == 1);
        } else if ((fishRowN == SquareSideSize) && (fishColN == SquareSideSize)) {
            // Fish on the right bottom corner.
            do {
                direction = getRandomInt(1, 4);
                repeat = 0;
                if ((direction == 1) && (fullElements[fishRowN-1][fishColN] == 0)) {
                    eachFish.rowN = fishRowN-1;
                    break;
                } else if ((direction == 2) && (fullElements[fishRowN][1] == 0)) {
                    eachFish.colN = 1;
                    break;
                } else if ((direction == 3) && (fullElements[1][fishColN] == 0)) {
                    eachFish.rowN = 1;
                    break;
                } else if ((direction == 4) && (fullElements[fishRowN][SquareSideSize-1] == 0)) {
                    eachFish.colN = SquareSideSize-1;
                    break;
                } else if ((fullElements[fishRowN-1][fishColN] == 0) || (fullElements[fishRowN][1] == 0)
                        || (fullElements[1][fishColN] == 0) || (fullElements[fishRowN][SquareSideSize-1] == 0)) {

                    repeat = 1;
                } else {
                    direction = 5;
                    break;
                }
            } while (repeat == 1);
        } else if (fishRowN == 1) {
            // Fish on the top line.
            do {
                direction = getRandomInt(1, 4);
                repeat = 0;
                if ((direction == 1) && (fullElements[SquareSideSize][fishColN] == 0)) {
                    eachFish.rowN = SquareSideSize;
                    break;
                } else if ((direction == 2) && (fullElements[fishRowN][fishColN+1] == 0)) {
                    eachFish.colN = fishColN+1;
                    break;
                } else if ((direction == 3) && (fullElements[fishRowN+1][fishColN] == 0)) {
                    eachFish.rowN = fishRowN+1;
                    break;
                } else if ((direction == 4) && (fullElements[fishRowN][fishColN-1] == 0)) {
                    eachFish.colN = fishColN-1;
                    break;
                } else if ((fullElements[SquareSideSize][fishColN] == 0) || (fullElements[fishRowN][fishColN+1] == 0)
                        || (fullElements[fishRowN+1][fishColN] == 0) || (fullElements[fishRowN][fishColN-1] == 0)) {

                    repeat = 1;
                } else {
                    direction = 5;
                    break;
                }
            } while (repeat == 1);
        } else if (fishRowN == SquareSideSize) {
            // Fish on the bottom line.
            do {
                direction = getRandomInt(1, 4);
                repeat = 0;
                if ((direction == 1) && (fullElements[fishRowN-1][fishColN] == 0)) {
                    eachFish.rowN = fishRowN-1;
                    break;
                } else if ((direction == 2) && (fullElements[fishRowN][fishColN+1] == 0)) {
                    eachFish.colN = fishColN+1;
                    break;
                } else if ((direction == 3) && (fullElements[1][fishColN] == 0)) {
                    eachFish.rowN = 1;
                    break;
                } else if ((direction == 4) && (fullElements[fishRowN][fishColN-1] == 0)) {
                    eachFish.colN = fishColN-1;
                    break;
                } else if ((fullElements[fishRowN-1][fishColN] == 0) || (fullElements[fishRowN][fishColN+1] == 0)
                        || (fullElements[1][fishColN] == 0) || (fullElements[fishRowN][fishColN-1] == 0)) {

                    repeat = 1;
                } else {
                    direction = 5;
                    break;
                }
            } while (repeat == 1);
        } else if (fishColN == 1) {
            // Fish on the left line.
            do {
                direction = getRandomInt(1, 4);
                repeat = 0;
                if ((direction == 1) && (fullElements[fishRowN-1][fishColN] == 0)) {
                    eachFish.rowN = fishRowN-1;
                    break;
                } else if ((direction == 2) && (fullElements[fishRowN][fishColN+1] == 0)) {
                    eachFish.colN = fishColN+1;
                    break;
                } else if ((direction == 3) && (fullElements[fishRowN+1][fishColN] == 0)) {
                    eachFish.rowN = fishRowN+1;
                    break;
                } else if ((direction == 4) && (fullElements[fishRowN][SquareSideSize] == 0)) {
                    eachFish.colN = SquareSideSize;
                    break;
                } else if ((fullElements[fishRowN-1][fishColN] == 0) || (fullElements[fishRowN][fishColN+1] == 0)
                        || (fullElements[fishRowN+1][fishColN] == 0) || (fullElements[fishRowN][SquareSideSize] == 0)) {

                    repeat = 1;
                } else {
                    direction = 5;
                    break;
                }
            } while (repeat == 1);
        } else if (fishColN == SquareSideSize) {
            // Fish on the right line.
            do {
                direction = getRandomInt(1, 4);
                repeat = 0;
                if ((direction == 1) && (fullElements[fishRowN-1][fishColN] == 0)) {
                    eachFish.rowN = fishRowN-1;
                    break;
                } else if ((direction == 2) && (fullElements[fishRowN][1] == 0)) {
                    eachFish.colN = 1;
                    break;
                } else if ((direction == 3) && (fullElements[fishRowN+1][fishColN] == 0)) {
                    eachFish.rowN = fishRowN+1;
                    break;
                } else if ((direction == 4) && (fullElements[fishRowN][fishColN-1] == 0)) {
                    eachFish.colN = fishColN-1;
                    break;
                } else if ((fullElements[fishRowN-1][fishColN] == 0) || (fullElements[fishRowN][1] == 0)
                        || (fullElements[fishRowN+1][fishColN] == 0) || (fullElements[fishRowN][fishColN-1] == 0)) {

                    repeat = 1;
                } else {
                    direction = 5;
                    break;
                }
            } while (repeat == 1);
        } else {
            // Fish on the center desk.
            do {
                direction = getRandomInt(1, 4);
                repeat = 0;
                if ((direction == 1) && (fullElements[fishRowN-1][fishColN] == 0)) {
                    eachFish.rowN = fishRowN-1;
                    break;
                } else if ((direction == 2) && (fullElements[fishRowN][fishColN+1] == 0)) {
                    eachFish.colN = fishColN+1;
                    break;
                } else if ((direction == 3) && (fullElements[fishRowN+1][fishColN] == 0)) {
                    eachFish.rowN = fishRowN+1;
                    break;
                } else if ((direction == 4) && (fullElements[fishRowN][fishColN-1] == 0)) {
                    eachFish.colN = fishColN-1;
                    break;
                } else if ((fullElements[fishRowN-1][fishColN] == 0) || (fullElements[fishRowN][fishColN+1] == 0)
                        || (fullElements[fishRowN+1][fishColN] == 0) || (fullElements[fishRowN][fishColN-1] == 0)) {

                    repeat = 1;
                } else {
                    direction = 5;
                    break;
                }
            } while (repeat == 1);
        }

        // Clearing place where fish was.
        paintAnimal(fishRowN, fishColN, '');
        fullElements[fishRowN][fishColN] = 0;

        // Borning a fish child.
        if ((eachFish.fishLifeTime != 0) && (eachFish.fishLifeTime % fishBornPeriod == 0) && (direction != 5)) {
            var newFishObj = {};
            newFishObj.colN = fishColN;
            newFishObj.rowN = fishRowN;
            newFishObj.fishLifeTime = 0;
            paintAnimal(fishRowN, fishColN, fishColor);
            newFishChild.push(newFishObj);
            fullElements[fishRowN][fishColN] = 2;
        }

        // Count fish lifetime.
        eachFish.fishLifeTime++;

        // Painting place where fish will be.
        paintAnimal(eachFish.rowN, eachFish.colN, fishColor);
        fullElements[eachFish.rowN][eachFish.colN] = 2;

        // Fish dying.
        if (eachFish.fishLifeTime == fishLifeTime) {
            paintAnimal(eachFish.rowN, eachFish.colN, '');
            fullElements[eachFish.rowN][eachFish.colN] = 0;
            if ((fishKey = array_search(eachFish, Fishes)) !== false) {
                deleteFishArray.push(fishKey);
            }
        }
    });

    // Sorting an array for right deleting fishes.
    deleteFishArray.sort(sortfunction);
    for (var i=0; i<deleteFishArray.length; i++) {
        Fishes.splice((deleteFishArray[i]-i), 1);
    }
    deleteFishArray.splice(0, deleteFishArray.length);

    for (key in newFishChild) {
        Fishes.push(newFishChild[key]);
    }
    newFishChild = new Array();

    // Drawing on the canvas results.
    timeCounter++;
    var canvas = document.getElementById("live_graph");
    var context = canvas.getContext("2d");
    // Shark style.
    context.beginPath();
    context.strokeStyle = sharkColor;
    context.lineWidth = 2;
    context.moveTo(sharkSumCoordinates[0], sharkSumCoordinates[1]);
    context.lineTo(timeCounter, (390-Sharks.length));
    context.stroke();
    sharkSumCoordinates = [timeCounter, (390-Sharks.length)];

    // Counting Miu constant.
        // For Shark.
    if (timeCounter == 60 && Sharks.length != 0 && Fishes.length != 0) {
        MiuShark[0] = Math.log((NSharks*(K - Sharks.length))/(Sharks.length*(K-NSharks)))/(0-50);
        MiuFish[0] = Math.log((NFishes*(K - Fishes.length))/(Fishes.length*(K-NFishes)))/(0-50);
    } else if (timeCounter == 160 && Sharks.length != 0 && Fishes.length != 0) {
        MiuShark[1] = Math.log((NSharks*(K - Sharks.length))/(Sharks.length*(K-NSharks)))/(0-150);
        MiuFish[1] = Math.log((NFishes*(K - Fishes.length))/(Fishes.length*(K-NFishes)))/(0-150);
    } else if (timeCounter == 260 && Sharks.length != 0 && Fishes.length != 0) {
        MiuShark[2] = Math.log((NSharks*(K - Sharks.length))/(Sharks.length*(K-NSharks)))/(0-250);
        MiuFish[2] = Math.log((NFishes*(K - Fishes.length))/(Fishes.length*(K-NFishes)))/(0-250);
    } else if (timeCounter == 261 && MiuShark.length == 3) {
        MiuShark[0] = (MiuShark[0] + MiuShark[1] + MiuShark[2])/3;
        paintStaticSharkGraph(MiuShark[0]);
        MiuShark = new Array();

        MiuFish[0] = (MiuFish[0] + MiuFish[1] + MiuFish[2])/3;
        paintStaticFishGraph(MiuFish[0]);
        MiuFish = new Array();
    }
        // For Fish.

    // Fish style.
    context.beginPath();
    context.strokeStyle = fishColor;
    context.lineWidth = 2;
    context.moveTo(fishSumCoordinates[0], fishSumCoordinates[1]);
    context.lineTo(timeCounter, (390-Fishes.length));
    context.stroke();
    fishSumCoordinates = [timeCounter, (390-Fishes.length)];

    timer = setTimeout("startLiveProcess()", timerMiliseconds);
}


// Paint static Shark graph.
function paintStaticSharkGraph(Miu) {
    var canvas = document.getElementById("static_graph");
    var context = canvas.getContext("2d");

    context.beginPath();
    context.lineWidth = 2;
    context.strokeStyle = sharkColor;
    context.moveTo(10, (390-NSharks));
    K = SquareSideSize*SquareSideSize;
    for (var i=11; i<=990; i++) {
        N = (K*NSharks)/(NSharks+(K-NSharks)*Math.exp(Miu*(0-(i-10))));
        context.lineTo(i, (390-N));
    }
    context.stroke();
}

// Paint static Fish graph.
function paintStaticFishGraph(Miu) {
    var canvas = document.getElementById("static_graph");
    var context = canvas.getContext("2d");

    context.beginPath();
    context.lineWidth = 2;
    context.strokeStyle = fishColor;
    context.moveTo(10, (390-NFishes));
    K = SquareSideSize*SquareSideSize;
    for (var i=11; i<=990; i++) {
        N = (K*NFishes)/(NFishes+(K-NFishes)*Math.exp(Miu*(0-(i-10))));
        context.lineTo(i, (390-N));
    }
    context.stroke();
}



function paintAnimal(rowN, colN, color) {
    element = document.getElementById(rowN+'|'+colN);
    element.className = color.replace('#', 'fish-').toLowerCase();
}

function getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function stopLivingProcess() {
    clearTimeout(timer);
    timer = undefined;
}

function startLivingProcess() {
    // prevents errors on pressing start before setup
    insertParameters();
    if (!timer) {
        timer = setTimeout("startLiveProcess()", timerMiliseconds);
    }
}

function array_search( needle, haystack, strict ) { // Searches the array for a given value and returns the corresponding key if
    // successful

    var strict = !!strict;

    for(var key in haystack){
        if( (strict && haystack[key] === needle) || (!strict && haystack[key] == needle) ){
            return key;
        }
    }

    return false;
}

// Function for sorting an array with numbers.
function sortfunction(a, b) {
    return (a - b);
}